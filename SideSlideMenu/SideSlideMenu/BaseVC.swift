//
//  BaseVC.swift
//  SideSlideMenu
//
//  Created by Alvin Luu on 8/8/18.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation

class BaseVC: UIViewController {
    
    
    @IBAction func backAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let revealVC = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
//        self.present(revealVC, animated: true) {
        self.dismiss(animated: true) {
            
        }
        
        
    }
}
