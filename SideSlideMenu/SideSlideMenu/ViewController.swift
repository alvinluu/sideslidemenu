//
//  ViewController.swift
//  SideSlideMenu
//
//  Created by Alvin Luu on 8/7/18.
//  Copyright © 2018 None. All rights reserved.
//
// https://www.youtube.com/watch?v=8EFfPT3UeWs

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var openBtn: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        openBtn.target = self.revealViewController()
        openBtn.action = #selector(SWRevealViewController.rightRevealToggle(_:))
//        openBtn.action = #selector(SWRevealViewController.revealToggle(_:))
        
        
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

