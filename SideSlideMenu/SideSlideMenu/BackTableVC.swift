//
//  BackTableVC.swift
//  SideSlideMenu
//
//  Created by Alvin Luu on 8/7/18.
//  Copyright © 2018 None. All rights reserved.
//

import Foundation

class BackTableVC: UITableViewController {
    
    var tableArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableArray = ["HelloVC", "SecondVC", "WorldVC"]
        
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        cell.textLabel?.text = tableArray[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let nameView = tableArray[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destVC = storyboard.instantiateViewController(withIdentifier: nameView)
        self.present(destVC, animated: true)
    }
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        let destVC = segue.destination as! ViewController
//        
//        let indexPath: IndexPath = self.tableView.indexPathForSelectedRow!
//        
//        destVC.varView = indexPath.row
//    }
}
